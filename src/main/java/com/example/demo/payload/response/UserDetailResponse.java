package com.example.demo.payload.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailResponse {
  @NonNull
  private Long id;

  @NonNull
  private String username;

  @NonNull
  private String email;  
}
